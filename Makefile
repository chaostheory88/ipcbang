CC:=gcc
CFLAGS:=-O2 -Wall
SOURCES:=ipcbang.c main.c
HEADERS:=ipcbang.h demo_shellcode.h
EXECUTABLE:=ipcbang

all: $(HEADERS) $(SOURCES) $(EXECUTABLE)

$(EXECUTABLE): $(HEADERS) $(SOURCES)
	$(CC) $(CFLAGS) $(SOURCES) $(HEADERS) -o $@ 

clean:
	rm -rf $(EXECUTABLE)