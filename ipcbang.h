/*
  Copyright (C) 2013  Fabrizio Curcio

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  as published by the Free Software Foundation; either version 2
  of the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

#ifndef IPCBANG_H
#define IPCBANG_H

#include <stdarg.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <pwd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/shm.h>
#include <sys/ipc.h>
#include <sys/mman.h>

#include "demo_shellcode.h"

#define _GNU_SOURCE
#define _XOPEN_SOURCE

void err_exit(const char *format, ...);

void exit_detach(char *mem);

void change_perms(int shmid, mode_t perms);

void enum_vuln_mem(mode_t perms);

void write_mem(char *dst, char *src, size_t size, off_t offset, size_t mem_size);

void crash(char *dst, char c, size_t size);

void dump_mem(char *src, size_t size, off_t offset, size_t mem_size);

void dump_to_file(const char *file, char *src, size_t size);

void dump_segment(int shmid, struct shmid_ds ds);

void exec_shellcode(const char *shellcode_file, char *mem, size_t mem_size, off_t offset);

#endif
